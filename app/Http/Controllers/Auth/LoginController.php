<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // Login
    public function showLoginForm()
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/auth/login', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function checkLogin(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'employee_no' => 'required',
            'employee_password' => 'required|min:4|max:25',
        ]);

        $employee_no = $request->employee_no;
        $password = $request->employee_password;
        $query = "SELECT * FROM auth WHERE AUTH_CODE = '$employee_no'";
        $userInfo = collect(DB::select($query))->first();
        // dd($userInfo);

        if (!$userInfo) {
            return back()->with('fail', 'ไม่พบผูัใช้งาน');
        } else {
            // check password

            if ($password == $userInfo->AUTH_PWD) {

                $request->session()->put('LoggedUser', $userInfo);
                $ipaddr = request()->ip();
                return redirect('/home');
            } else {
                // dd('ไม่ตรงกัน');
                return back()->with('fail', 'รหัสพนักงาน หรือ รหัสผ่านไม่ถูกต้อง');
            }
        }
    }

    public function logout()
    {
        if (session()->has('LoggedUser')) {
            session()->pull('LoggedUser');

            return redirect('/');
        }
    }
}
