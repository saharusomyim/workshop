<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class fontStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageConfigs = ['sidebarCollapsed' => true];
        // $pageConfigs = ['showMenu' => false];

        // เรียกข้อมูลสินค้า
        $query = "SELECT * FROM products A LEFT JOIN product_type B ON A.p_type = B.pdt_id ORDER BY B.pdt_no , A.p_id ASC";
        $dataProducts = DB::select($query);

        //เรียกข้อมูลประเภทสินค้า
        $query = "SELECT * FROM product_type";
        $dataProductType = DB::select($query);

        //เรียกข้อมูล ราคาบวกเพิ่มเมนู ร้อน-เย็น
        $query = "SELECT * FROM alpha WHERE alpha_type = 'addOption'";
        $dataAddOption = DB::select($query);

        // dd($dataAddOption);
        return view('/content/fontStore/index', [
            'pageConfigs' => $pageConfigs,
            'dataProducts' => $dataProducts,
            'dataProductType' => $dataProductType,
            'dataAddOption' => $dataAddOption
        ]);
    }

    public function getProductsFormType(Request $request)
    {
        $request->validate([
            'menuType' => 'required',
        ]);

        $menuType = !empty($request->menuType) ? $request->menuType : '';
        //เรียกข้อมูลสินค้า
        $query = "SELECT * FROM products A LEFT JOIN product_type B ON A.p_type = B.pdt_id WHERE B.pdt_id = $menuType ORDER BY B.pdt_no , A.p_id ASC";
        $dataProducts = DB::select($query);
        // dd($menuType);

        $html = '';
        foreach ($dataProducts as $item) {
            $html .= '<div class="col-lg-3 col-md-4 item-id">';
            $html .= '<div class="card add-to-cart" data-itemId=" ' . $item->p_id . ' " data-option=" ' . $item->p_option . '">';
            $html .= '<img class="card-img-top item-product no-padding container-fluid" src="'.(!empty($item->p_img) ? $item->p_img : 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132484366.jpg').'" alt="Card image cap" />';
            $html .= '<div class="card-body">';
            $html .= '<h4 class="card-title item-name" style="height: 30px">' . $item->p_name . ' </h4>';
            $html .= '<p class="card-text item-price">' . $item->p_price . '  <b>฿/ชิ้น</b></p>';
            $html .= '<p class="card-text item-qty">คงเหลือ ' . $item->p_qty . ' <b>ชิ้น</b></p>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div> ';
        }


        return $html;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
