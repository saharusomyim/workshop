<?php

use Illuminate\Support\Facades\Route;

//import Controller Pages
use App\Http\Controllers\fontStoreController;




//route

//หน้าแรก
Route::get('/', [fontStoreController::class, 'index'])->name('homePage');
Route::get('home', [fontStoreController::class, 'index'])->name('home');

//หน้าขายหน้าร้าน
Route::get('fontStore', [fontStoreController::class, 'index'])->name('fontStore');

//get Products form Type
Route::post('getProductsFormType', [fontStoreController::class, 'getProductsFormType'])->name('getProductsFormType');

