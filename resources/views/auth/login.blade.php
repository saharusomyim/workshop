@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
    @php
        // if (!empty(session('LoggedUser'))) {
        //     // redirect()->route('home');
        //     echo ("<script>location.href='home'</script>");
        //     // dd('ss');
        // } else {
        //     // dd('sss');
        //     echo ("<script>location.href='login'</script>");
        //     // return redirect()->route('login');
        // }
    @endphp
    <div class="auth-wrapper auth-v1 px-2">
        <div class="auth-inner py-2">
            <!-- Login v1 -->
            <div class="card mb-0">
                <div class="card-body">
                    <div class="d-flex justify-content-center">
                        <img src="{{ asset('/images/logo/Bakery_Logo.jpg') }}" width="180px" alt="">
                    </div>
                    {{-- <div class="d-flex justify-content-center">
                        <h2 class="brand-text text-primary ml-1">เข้าสู่ระบบ POS</h2>
                    </div> --}}
                    <form class="auth-login-form mt-2" method="POST" action="{{ route('checkLogin') }}">
                        @csrf
                        <div class="form-group">
                            <label for="employee_no" class="form-label">รหัสพนักงาน</label>
                            <input type="number" class="form-control form-control-lg" id="employee_no" name="employee_no"
                                placeholder="รหัสพนักงาน" aria-describedby="employee_no" tabindex="1" autofocus
                                value="" autofocus />
                        </div>

                        <div class="form-group">
                            <div class="d-flex justify-content-between">
                                <label for="login-password">รหัสผ่าน</label>
                            </div>
                            <div class="input-group input-group-merge form-password-toggle">
                                <input type="password" class="form-control form-control-merge form-control-lg" id="employee_password"
                                    name="employee_password" tabindex="2"
                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                    aria-describedby="employee_password" />
                                <div class="input-group-append">
                                    <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary btn-block" tabindex="4">เข้าสู่ระบบ <i
                                data-feather='log-in'></i></button>
                    </form>
                </div>
            </div>
            <!-- /Login v1 -->
        </div>
    </div>
@endsection
