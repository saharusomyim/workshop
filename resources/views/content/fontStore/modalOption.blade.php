<div class="modal fade text-left" id="showOption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel4">Option</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <input type="hidden" id="optionData">
                <input type="hidden" id="option_p_id">
                <input type="hidden" id="option_p_name">
                <input type="hidden" id="option_p_price">
                <input type="hidden" id="option_addOptionType" value="{{ $dataAddOption[0]->alpha_value }}">

                <div class="row">
                    <div class="col-md-12 optionLevel_3">
                        <h3>ประเภท</h3>

                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-outline-primary active" data-menuType="1">
                                <input type="radio" name="optionType" value="hot" checked /> <i
                                    class="fa-brands fa-hotjar"></i>
                                ร้อน +5 บาท
                            </label>
                            <label class="btn btn-outline-primary" data-menuType="2">
                                <input type="radio" name="optionType" value="cool" /> <i
                                    class="fa-solid fa-snowflake"></i> เย็น
                                +5 บาท
                            </label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-12 optionLevel_3">
                        <h3>ระดับความหวาน</h3>

                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-outline-primary active" data-menuType="1">
                                <input type="radio" name="optionSugar" value="lavel_1" /> หวานมาก
                            </label>
                            <label class="btn btn-outline-primary" data-menuType="2">
                                <input type="radio" name="optionSugar" value="lavel_2" />
                                หวานกลาง
                            </label>
                            <label class="btn btn-outline-primary" data-menuType="2">
                                <input type="radio" name="optionSugar" value="lavel_3" />
                                หวานน้อย
                            </label>
                        </div>

                        <hr>
                    </div>

                    <div class="col-md-12">
                        <h3>ภาชนะ</h3>
                        <div class="d-flex">
                            <div class="custom-control custom-control-primary custom-checkbox mr-2">
                                <input type="checkbox" class="custom-control-input" id="straw" checked />
                                <label class="custom-control-label" for="straw">หลอด</label>
                            </div>

                            <div class="custom-control custom-control-primary custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="glassCover" />
                                <label class="custom-control-label" for="glassCover">ฝาแก้ว</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-block btnAddProductFormOption" data-dismiss="modal"><i
                        class="fa-regular fa-circle-check"></i> ยืนยัน</button>
            </div>
        </div>
    </div>
</div>
