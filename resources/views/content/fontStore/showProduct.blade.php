@foreach ($dataProducts as $item)
    <div class="col-lg-3 col-md-4 item-id">
        <div class="card add-to-cart" data-itemId="{{ $item->p_id }}" data-option=" {{ $item->p_option }}">
            <img class="card-img-top item-product no-padding container-fluid"
                src='{{ !empty($item->p_img) ? $item->p_img : 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132484366.jpg' }}'
                alt="Card image cap" />
            <div class="card-body">
                <h4 class="card-title item-name" style="height: 30px">{{ $item->p_name }}</h4>
                <p class="card-text item-price">{{ $item->p_price }} <b>฿/ชิ้น</b></p>
                <p class="card-text item-qty">คงเหลือ {{ $item->p_qty }} <b>ชิ้น</b></p>
            </div>
        </div>
    </div>
@endforeach
