<style>

    .btnNumberGetMoney {
        width: 100%;
        height: 90px;
        font-size: 32px;
        background-color: white;
        border: none;
        color: #273342;
        border: 0.5px solid rgb(238, 238, 238);
        /* border-radius: 10px; */
        margin: 0px;
    }

    .btnClearGetMoney{
        width: 100%;
        height: 90px;
        font-size: 32px;
        background-color: white;
        border: none;
        color: #273342;
        border: 0.5px solid rgb(238, 238, 238);
        /* border-radius: 10px; */
        margin: 0px;
    }

    .btnDelNumberGetMoney{
        width: 100%;
        height: 90px;
        font-size: 32px;
        background-color: white;
        border: none;
        color: #273342;
        border: 0.5px solid rgb(238, 238, 238);
        /* border-radius: 10px; */
        margin: 0px;
    }

    .btnNumberGetMoneyFull{
        width: 100%;
        height: 90px;
        font-size: 32px;
        background-color: white;
        border: none;
        color: #273342;
        border: 0.5px solid rgb(238, 238, 238);
        /* border-radius: 10px; */
        margin: 0px;
    }

    .btnNumberFull{
        width: 100%;
        height: 90px;
        font-size: 32px;
        background-color: white;
        border: none;
        color: #f9a912;
        border: 0.5px solid rgb(238, 238, 238);
        /* border-radius: 10px; */
        margin: 0px;
    }

    .btnNumberFull{
        color:f9a912;
    }
    #inputLabelGetMoney {
        height: 100px;
        font-size: 60px;
        vertical-align: bottom;
        text-align: right;
        background-color: #27334210;
        color: #f9a912;
        padding: 0 10px 0 0;
        font-family: 'Poppins', sans-serif;
        border-top-radius: 20px;
    }


    .btnsubmitGetMoney{
        font-size: 20px;
    }
</style>
<meta name="viewport" content="target-densitydpi=device-dpi, initial-scale=1.0, user-scalable=no" />
<div class="modal fade text-left" id="modalGetMoney" tabindex="-1" role="dialog" aria-labelledby="modalGetMoney"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="padding:0px;">

                <div class="d-flex flex-column">
                    <input type="hidden" id="inputTotalGetMoney" value="0">
                    <div class="d-flex justify-content-end" id="inputLabelGetMoney">0
                    </div>
                    <div class="d-flex">
                        <button class="btnClearGetMoney" style="color:#f9a912">เคลียร์</button>
                    </div>
                    <div class="d-flex">
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="7">7</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="8">8</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="9">9</button>
                        <button class="btnNumberFull" data-valueNumberGetMoney="1000">1000</button>
                    </div>
                    <div class="d-flex">
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="4">4</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="5">5</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="6">6</button>
                        <button class=" btnNumberFull" data-valueNumberGetMoney="500">500</button>
                    </div>
                    <div class="d-flex">
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="1">1</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="2">2</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="3">3</button>
                        <button class=" btnNumberFull" data-valueNumberGetMoney="100">100</button>
                    </div>
                    <div class="d-flex">
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="0">0</button>
                        <button class="btnNumberGetMoney" data-valueNumberGetMoney="00">00</button>
                        <button class="btnDelNumberGetMoney" style="color:#ff4e4e"><i class="fas fa-backspace"></i></button>
                        <button class="btnNumberGetMoneyFull" style="background-color: #f9a912; color:#ffff">เติมจำนวน</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding:0px;">
                <button type="button" class="btn btn-dark btn-lg btn-block btnsubmitGetMoney" data-dismiss="modal" style="font-size:36px; height:80px; background-color: #243e7e !important;">ยืนยันรับงิน <i class="far fa-check-circle"></i></button>
            </div>
        </div>
    </div>
</div>
