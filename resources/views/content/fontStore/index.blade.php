@extends('layouts/contentLayoutMaster')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/swiper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce-details.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-number-input.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .showNameToCard {
            font-size: 20px;
        }

        .showToCard {
            padding-top: 10px;
            padding-bottom: 10px;
            border-bottom: 1px solid #EEEEEE;
        }

        .showSumTotalToCard {
            font-size: 20px;
            margin-top: 20px;
            font-weight: 800;

        }

        .custom-btn-payment {
            font-size: 20px;
        }

        .showProducts {
            max-height: 500px;
            overflow: auto;
        }

        .showTotolPrice {
            max-height: 400px;
            overflow: auto;
        }

        .no-padding {
            padding-left: 0;
            padding-right: 0;
        }

        /* // Large devices (desktops, 992px and up) */
        /* @media (min-width: 1200px) {
                                                                                        .showProducts {
                                                                                            max-height: 100%;
                                                                                            overflow: auto;
                                                                                        }

                                                                                        .showTotolPrice {
                                                                                            max-height: 100%;
                                                                                            overflow: auto;
                                                                                        }
                                                                                    } */



        .sumTotalCard {
            position: absolute;
            right: 0;
            bottom: 20px;

        }

        .swal2-close {
            color: #e95858 !important;
            border: 2px solid !important;
            margin: 2px !important;
        }
    </style>
@endsection


{{-- @section('title', 'ข้อมูลสินค้า') --}}
@section('content')
    <div class="d-flex justify-content-end mb-1">


    </div>
    <div class="row gridShowProducts">
        <div class="col-md-7 col-lg-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body d-flex justify-content-between">

                            <div>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-outline-primary active btnMenu" data-menuType="1">
                                        <input type="radio" name="radio_options" id="btnCoffee" checked /> <i
                                            class="fa-solid fa-mug-hot"></i> Coffee
                                    </label>
                                    <label class="btn btn-outline-primary btnMenu" data-menuType="2">
                                        <input type="radio" name="radio_options " id="btnTea" /> <i
                                            class="fa-solid fa-mug-saucer"></i> Tea
                                    </label>
                                    <label class="btn btn-outline-primary btnMenu" data-menuType="3">
                                        <input type="radio" name="radio_options " id="btnSoftDrink" /> <i
                                            class="fas fa-cocktail"></i> Soft drink
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row showProducts">
                <!-- สินค้า-->
                @include('content.fontStore.showProduct')
                <!--  สินค้า-->
            </div>
        </div>

        <div class="col-md-5 col-lg-4">
            <div class="card p-1 d-flex flex-column" id="shopping-cart" style="height: 100%;">
                <div class="row">
                    <div class="items showTotolPrice col-md-12">
                        <div class="d-flex justify-content-center">
                            <img class="imgEmptyCard" src="{{ asset('/images/logo/empty_card.png') }}" width="250px"
                                alt="">
                        </div>
                    </div>

                    <div class="sumTotalCard col-md-12">
                        <div class="total-price d-flex align-items-end showSumTotalToCard">ราคารวม: 0 บาท</div>
                        <span>ส่วนลด: 0 บาท</span>
                        <input type="hidden" id="subTotalToCard">
                        <hr>
                        <button type="button"
                            class="btn btn-success waves-effect waves-float waves-light custom-btn-payment col-md-12 btnPayment"><i
                                class="fas fa-money-check-alt"></i>
                            ชำระเงิน</button>

                    </div>
                </div>

                <!-- modal Option -->
                @include('content.fontStore.modalOption')
                <!-- modal Option -->

                <!-- modal รับเงิน-->
                @include('content.fontStore.modalGetMoney')
                <!--  modal รับเงิน-->


            </div>
        </div>

    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/swiper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-details.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-number-input.js')) }}"></script>


    {{-- Page js files --}}
    <script>
        $(document).ready(function() {

            //เลือกประเภทสินค้า
            $('.btnMenu').click(function() {
                var getmenuType = $(this).attr("data-menuType");
                // console.log(getmenuType)

                //เรียกข้อมูลสินค้าจากประเภทสินค้า
                $.ajax({
                    type: "POST",
                    url: "{{ route('getProductsFormType') }}",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        'menuType': getmenuType,

                    },
                    success: function(result) {
                        // console.log(result);
                        $('.showProducts').empty();
                        $(".showProducts").append(result);
                    },
                    error: function(xhr, status, error) {
                        console.log("getDataJSTree : xhr", xhr.responseText);
                        // resolve(xhr.responseJSON.exception);
                        console.log(xhr.responseJSON.exception)
                        return;
                    }
                })
            })


            // Add เพิ่มสินค้าไป ตะกร้า
            // $('.add-to-cart').click(function() {
            $('.showProducts').on('click', '.add-to-cart', function() {
                var dataOption = $(this).attr("data-option");
                var productCode = $(this).attr("data-itemId");
                var item = $(this).parent();
                var name = $(item).find('.item-name').text();
                var price = $(item).find('.item-price').text().replace('฿/ชิ้น', '');
                var imgProduct = $(item).find('.item-product').attr('src');


                // ถ้ามี Option ให้เลือก option ก่อน ถ้าไม่มีให้เพิ่มในตะกร้าสินค้าทันที
                if (dataOption > 0) {
                    showOption(dataOption, productCode, name, price);
                }
            });

            function showOption(dataOption, productCode, name, price) {
                // console.log(dataOption)
                $('#showOption').modal('show');
                $('#optionData').val(dataOption);
                $('#option_p_id').val(productCode);
                $('#option_p_name').val(name);
                $('#option_p_price').val(price);

                // เชค Option
                if (dataOption < 2) {
                    $('.optionLevel_3').hide();
                } else {
                    $('.optionLevel_3').show();
                }
            }

            $('.btnAddProductFormOption').click(function() {
                var optionData = $('#optionData').val();
                var productCode = $('#option_p_id').val();
                var productname = $('#option_p_name').val();
                var productprice = $('#option_p_price').val();
                var option_addOptionType = $('#option_addOptionType').val();
                var optionType = $("input[name='optionType']:checked").val();
                var optionSugar = $("input[name='optionSugar']:checked").val();
                var straw = ($('#straw').is(':checked') ? true : false);
                var glassCover = ($('#glassCover').is(':checked') ? true : false);

                var sumPrice = parseInt(productprice) + parseInt(option_addOptionType);

                // console.log(optionSugar)

                addItemToCart(optionData, productCode, productname, optionType, optionSugar, straw,
                    glassCover, sumPrice);
            })

            // ลบสินค้าใน ตะกร้า
            // $('.remove-item').click(function() {
            $('#shopping-cart').on('click', '.remove-item', function() {
                var productCode = $(this).attr("data-productCode");
                var item = $(this).parent();
                var name = $(item).find('.item-name').text();
                // console.log(productCode)
                Swal.fire({
                    title: 'ต้องการลบสินค้า ?',
                    icon: 'warning',
                    showCancelButton: true,
                    reverseButtons: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ยืนยัน'
                }).then((result) => {
                    if (result.isConfirmed) {
                        removeItemFromCart(productCode);
                        Swal.fire(
                            'สำเร็จ !',
                            'ลบสินค้าสำเร็จ!',
                            'success'
                        )
                    }
                })

            });

            // อัพเดดราคาในตะกร้า
            function updateTotalPrice() {
                var total = 0;
                $('.showTotolPrice').each(function() {

                    jQuery.each($('.seqProduct'), function() {

                        total = total + parseInt($(this).attr("data-priceProduct") * $(this).val());
                    })
                });
                $('.total-price').text('รวมราคา: ฿' + total.toFixed(2));
                $('#subTotalToCard').val(total);

            }



            //ปุ่ม เพิ่มลดจำนวนในตะกร้า
            $('#shopping-cart').on('change', '.seqProduct', function() {
                updateTotalPrice();
            })

            // Add item to cart
            function addItemToCart(optionData, productCode, productname, optionType, optionSugar, straw, glassCover,
                sumPrice) {
                console.log(optionSugar)
                var itemHTML = '';
                var seqProduct = 0;
                //ปิดภาพกรุณาเลือกสินค้า
                $('.imgEmptyCard').hide();
                // $(".imgEmptyCard").attr("hidden",true);
                // $('.imgEmptyCard').css("display", "none");
                //เชคมีข้อมูลในตะกร้ารึยัง

                //สถานะ ร้อน-เย็น
                var statusOptionType = "danger";
                var nameOptionType = "";
                var nameOptionSugar = '';

                if (optionData > 1) {
                    if (optionType == "cool") {
                        statusOptionType = "info";
                        nameOptionType = 'เย็น';
                    } else {
                        nameOptionType = "ร้อน";
                    }

                    console.log(optionSugar)
                    if (optionSugar == 'lavel_1') {
                        nameOptionSugar = 'หวานมาก';
                    } else if (optionSugar == 'lavel_2') {
                        nameOptionSugar = 'หวานปกติ';
                    } else if (optionSugar == 'lavel_3') {
                        nameOptionSugar = 'หวานน้อย';
                    }
                }


                var statusStraw = '';
                if (straw == true) {
                    statusStraw = '<div class="badge badge-info">รับหลอด</div>';
                }

                var statusGlassCover = '';
                if (glassCover == true) {
                    statusGlassCover = '<div class="badge badge-info">รับฝาแก้ว</div>';
                }
                if ($('.showToCard').length == 0) {

                    console.log('เงือนไข showToCard.length');
                    itemHTML += '<div class="item showToCard" data-productCode="' + productCode + '">';
                    itemHTML += '<div class="d-flex justify-content-between align-items-center">';
                    itemHTML += '<div class="item-name showNameToCard">';
                    itemHTML += '' + productname + '';
                    itemHTML += '</div>';
                    itemHTML += '<div class="item-price showPriceToCard">';
                    itemHTML += 'ราคา ' + sumPrice + ' ฿/ชิ้น';
                    itemHTML += '</div>';
                    itemHTML += '</div>'; //end ชื่อ & ราคา
                    itemHTML += '<div class="d-flex justify-content-between align-items-center">';

                    itemHTML +=
                        '<div><button type="button" class="btn btn-danger btn-sm waves-effect waves-float waves-light remove-item mr-1" data-productCode="' +
                        productCode +
                        '"><i class="fas fa-trash-alt"></i></button><div class="badge badge-' + statusOptionType +
                        '">' + nameOptionType +
                        '</div>';
                    itemHTML += '<div class="badge badge-primary ml-1">' + nameOptionSugar +
                        '</div> ' + statusStraw + ' ' + statusGlassCover + '';
                    itemHTML += '</div>';
                    itemHTML += '<div class="d-flex align-items-center btnSeq">';
                    itemHTML += '<span>จำนวน</span>';
                    itemHTML += '<div class="input-group">';
                    itemHTML += '<input type="number" class="touchspin seqProduct' + productCode +
                        ' seqProduct" data-priceProduct="' + sumPrice + '" value="1" />';
                    itemHTML += '</div>';
                    itemHTML += '</div>';
                    itemHTML += '</div>'; //end ลบ & จำนวน
                    itemHTML += '</div>';
                    itemHTML += '</div>';
                } else {
                    // Else มีข้อมูลในตะกร้าอยู่แล้ว
                    var productCode_forLoop = 0;

                    //เชคว่ามีข้อมูลสินค้านั้นๆแล้วหรือไม่
                    $('.showToCard').each(function() {
                        if ($(this).attr("data-productCode") == productCode) {
                            productCode_forLoop = productCode;
                        }
                    })

                    //ถ้ามีสินค้าอยู่แล้วให้เพิ่มจำนวน
                    if (productCode_forLoop != '') {
                        seqProduct = parseInt($('.seqProduct' + productCode_forLoop).val()) + 1;
                        $('.seqProduct' + productCode_forLoop).val(seqProduct);
                    } else {
                        // Else ถ้าไม่มีให้เพิ่มสินค้าในตะกร้า
                        itemHTML += '<div class="item showToCard" data-productCode="' + productCode + '">';
                        itemHTML += '<div class="d-flex justify-content-between align-items-center">';
                        itemHTML += '<div class="item-name showNameToCard">';
                        itemHTML += '' + productname + '';
                        itemHTML += '</div>';
                        itemHTML += '<div class="item-price showPriceToCard">';
                        itemHTML += 'ราคา ' + sumPrice + ' ฿/ชิ้น';
                        itemHTML += '</div>';
                        itemHTML += '</div>'; //end ชื่อ & ราคา
                        itemHTML += '<div class="d-flex justify-content-between align-items-center">';
                        itemHTML +=
                            '<div><button type="button" class="btn btn-danger btn-sm waves-effect waves-float waves-light remove-item mr-1" data-productCode="' +
                            productCode +
                            '"><i class="fas fa-trash-alt"></i></button><div class="badge badge-' +
                            statusOptionType + '">' + nameOptionType +
                            '</div>' + statusStraw + ' ' + statusGlassCover + '';
                        itemHTML += '<div class="badge badge-primary ml-1">' + nameOptionSugar +
                            '</div>';
                        itemHTML += '</div>';
                        itemHTML += '<div class="d-flex align-items-center btnSeq">';
                        itemHTML += '<span>จำนวน</span>';
                        itemHTML += '<div class="input-group">';
                        itemHTML += '<input type="number" class="touchspin seqProduct' + productCode +
                            ' seqProduct" data-priceProduct="' + sumPrice + '" value="1" />';
                        itemHTML += '</div>';
                        itemHTML += '</div>';
                        itemHTML += '</div>'; //end ลบ & จำนวน
                        itemHTML += '</div>';
                        itemHTML += '</div>';
                    }

                    // })
                }


                $('.items').append(itemHTML);
                $(".touchspin").TouchSpin({
                    min: 1,
                    max: 900,
                    step: 1,
                    decimals: 0,
                }).on('change', function() {
                    //maybe optional
                });
                updateTotalPrice();
            }

            // Remove item from cart
            function removeItemFromCart(productCode) {

                $('.item').each(function() {
                    if ($(this).attr("data-productCode") == productCode) {
                        $(this).remove();
                    }
                });
                if ($('.showToCard').length == 0) {
                    $('.imgEmptyCard').show();
                }
                updateTotalPrice();
            }
        });


        $('.btnPayment').click(function() {
            if ($('.showToCard').length > 0) {
                $('#modalGetMoney').modal('show');
            } else {
                Swal.fire(
                    'ผิดพลาด',
                    'กรุณาเลือกสินค้าก่อนชำระเงิน',
                    'warning'
                )
            }
        })
        // แสดงรับเงิน
        $('.btnNumberGetMoney').click(function() {
            var valueBtn = $(this).attr("data-valueNumberGetMoney");
            var getShowValue = $('#inputTotalGetMoney').val();
            var totalGetMonet = 0;
            if (getShowValue == '0') {
                $('#inputTotalGetMoney').val('');
                $('#inputLabelGetMoney').empty();
                $('#inputTotalGetMoney').val(valueBtn);
                $('#inputLabelGetMoney').append(valueBtn);
            } else {
                totalGetMonet = getShowValue + valueBtn;
                // console.log('get' + getShowValue)
                // console.log('total' + valueBtn)
                $('#inputTotalGetMoney').val('');
                $('#inputLabelGetMoney').empty();
                $('#inputTotalGetMoney').val(totalGetMonet);
                $('#inputLabelGetMoney').append(totalGetMonet);
            }
        })

        //ปุ่มลบจำนวนเงิน
        $('.btnDelNumberGetMoney').click(function() {
            var getShowValue = $('#inputTotalGetMoney').val();
            var checkLength = getShowValue.length;
            // console.log(checkLength)
            var Total = getShowValue.substr(0, checkLength - 1);
            // console.log(Total)

            $('#inputTotalGetMoney').val('');
            $('#inputLabelGetMoney').empty();
            $('#inputTotalGetMoney').val(Total);
            $('#inputLabelGetMoney').append(Total);
        })

        //ปุ่มจำนวนเงินเต็ม
        $('.btnNumberFull').click(function() {
            var valueBtn = $(this).attr("data-valueNumberGetMoney");
            var getShowValue = $('#inputTotalGetMoney').val();
            var totalGetMonet = 0;

            totalGetMonet = parseInt(getShowValue) + parseInt(valueBtn);
            // console.log('get' + getShowValue)
            // console.log('total' + valueBtn)
            $('#inputTotalGetMoney').val('');
            $('#inputLabelGetMoney').empty();
            $('#inputTotalGetMoney').val(totalGetMonet);
            $('#inputLabelGetMoney').append(totalGetMonet);

        })

        //ปุ่มรับเงินเต็มจำนวน
        $('.btnNumberGetMoneyFull').click(function() {
            var getTotal = $('#subTotalToCard').val();
            $('#inputTotalGetMoney').val('');
            $('#inputLabelGetMoney').empty();
            $('#inputTotalGetMoney').val(getTotal);
            $('#inputLabelGetMoney').append(getTotal);
        })

        //ปุ้มเคลียจำนวนเงิน
        $('.btnClearGetMoney').click(function() {
            $('#inputTotalGetMoney').val('0');
            $('#inputLabelGetMoney').empty();
            $('#inputLabelGetMoney').append(0);
        })

        //ปุ่มยืนยันรับเงิน
        $('.btnsubmitGetMoney').click(function() {
            var getTotalToCard = $('#subTotalToCard').val();
            var getTotalToGetMoney = $('#inputTotalGetMoney').val();
            var sumTotal = parseInt(getTotalToGetMoney) - parseInt(getTotalToCard);

            // console.log('จำนวนเงินที่ต้องจ่าย' + parseInt(getTotalToCard))
            // console.log('รับจากลูกค้า' + parseInt(getTotalToGetMoney))
            if (parseInt(getTotalToGetMoney) < parseInt(getTotalToCard) || !getTotalToGetMoney) {
                Swal.fire({
                    icon: 'warning',
                    title: 'ผิดพลาด',
                    text: 'จำนวนเงินที่รับมาไม่พอ',
                })
            } else {

                Swal.fire({
                    title: 'สำเร็จ',
                    icon: 'success',
                    html: '<div style="font-size:36px; color:green;"><b>ทอน ' + sumTotal +
                        ' บาท</b></div> <div class="d-flex justify-content-center"><button type="button" class="btn btn-info btn-block" style="width:50%; height:50px; font-size:16px;"><i class="fas fa-print"></i> พิมพ์ใบเสร็จ</button></div>',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'ปิด',
                    allowOutsideClick: false,
                    showCloseButton: true,
                    focusCloseButton: true,
                }).then((result) => {
                    if (!result.isConfirmed) {
                        window.location.reload();
                    }
                })
            }
        })
    </script>
@endsection
