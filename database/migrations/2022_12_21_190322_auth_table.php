<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class AuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AUTH', function (Blueprint $table) {
            $table->string('AUTH_CODE',50)->nullable(true);
            $table->string('AUTH_PWD',30)->nullable(true);
            $table->string('AUTH_PREFIX',20)->nullable(true);
            $table->string('AUTH_FNAME',100)->nullable(true);
            $table->string('AUTH_LNAME',100)->nullable(true);
            $table->string('AUTH_LAVEL',1)->nullable(true);
            $table->string('AUTH_SEX',1)->nullable(true);
            $table->string('AUTH_PIX',255)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
