<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlphaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ALPHA', function (Blueprint $table) {
            $table->string('ALPHA_CODE',3)->nullable(true);
            $table->string('ALPHA_SET_TYPE',50)->nullable(true);
            $table->string('ALPHA_VAL',255)->nullable(true);
            $table->string('ALPHA_SEQ',3)->nullable(true);
            $table->string('ALPHA_STATUS',1)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
