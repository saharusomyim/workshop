<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PRODUCT', function (Blueprint $table) {
            $table->string('PRODUCT_CODE',3)->nullable(true);
            $table->string('PRODUCT_NAME',100)->nullable(true);
            $table->string('PRODUCT_VAL',3)->nullable(true);
            $table->string('PRODUCT_PRICE',3)->nullable(true);
            $table->string('PRODUCT_SEQ',3)->nullable(true);
            $table->string('PRODUCT_TYPE',50)->nullable(true);
            $table->string('PRODUCT_STATUS',1)->nullable(true);
            $table->string('PRODUCT_PIC',255)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
